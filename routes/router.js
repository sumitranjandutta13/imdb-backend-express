const e = require("express");
const express = require("express");
const db = require("../dbConnection");
require("dotenv").config();

const router = express.Router();

db.connect((err) => {
  if (err) {
    console.log("Error in Connection!!!");
  } else {
    console.log("Connection Done!!!");
  }
});

router.get("/all", (req, res) => {
  const sql =
    "Select m.movieid, m.name as movieName, m.year, m.plot, m.poster," +
    " a.name as actorName, p.name as prodName from movies m INNER JOIN" +
    " movieactor ma ON m.movieid = ma.movieid INNER JOIN actors a on ma.actorid" +
    " = a.actorid inner join movieproducer mp on m.movieid = mp.movieid" +
    " inner join producers p on mp.producerid = p.producerid;";
  db.query(sql, (err, data) => {
    if (err) {
      res.json({
        message: "Failed Server",
      });
    } else {
      res.json({
        message: "Success",
        data: data,
      });
    }
  });
});

router.get("/actors", (req, res) => {
  const sql = "Select actorid, name from actors";
  db.query(sql, (err, results, fields) => {
    if (err) {
      res.json({
        message: "Failed Server",
      });
    } else {
      res.json({
        message: "Success",
        data: results,
      });
    }
  });
});

router.get("/producers", (req, res) => {
  const sql = "Select producerid, name from producers";
  db.query(sql, (err, results, fields) => {
    if (err) {
      res.json({
        message: "Failed Server",
      });
    } else {
      res.json({
        message: "Success",
        data: results,
      });
    }
  });
});

router.post("/addactor", (req, res) => {
  console.log(req.body);
  db.query("INSERT INTO actors SET?", req.body, (err, results, fields) => {
    if (err) {
      res.json({
        message: "Failed",
      });
    } else {
      res.json({
        message: "success",
      });
    }
  });
});

router.post("/addproducer", (req, res) => {
  console.log(req.body);
  db.query("INSERT INTO producers SET?", req.body, (err, results, fields) => {
    if (err) {
      res.json({
        message: "Failed",
      });
    } else {
      res.json({
        message: "success",
      });
    }
  });
});

router.post("/addmovie", async (req, res) => {
  console.log(req.body);
  const moviedata = {};
  moviedata["name"] = req.body.name;
  moviedata["year"] = req.body.year;
  moviedata["plot"] = req.body.plot;
  moviedata["poster"] = req.body.poster;
  db.query("INSERT INTO movies SET?", moviedata, (err, results, fields) => {
    if (err) {
      res.json({
        message: "Failed",
      });
    }
  });
  db.query(
    "Select movieid from movies where name ='" + moviedata.name + "';",
    (err, results, fields) => {
      if (err) {
        res.json({
          message: "failed",
        });
      } else {
        const movieid = results[0].movieid;
        db.query(
          "INSERT INTO movieproducer SET?",
          {
            movieid: movieid,
            producerid: req.body.producer,
          },
          (err, results, fields) => {
            if (err) {
              console.log("i m working");
              res.json({
                message: "failed",
              });
            } else {
              req.body.actors.forEach((element) => {
                db.query(
                  "INSERT INTO movieactor SET?",
                  {
                    movieid: movieid,
                    actorid: element,
                  },
                  (err, results, fields) => {
                    if (err) {
                      console.log("i m working 2");
                      res.json({
                        message: "failed",
                      });
                    }
                  },
                );
              });
              res.json({
                message: "success",
              });
            }
          },
        );
      }
    },
  );
});

router.get("/:movieid", (req, res) => {
  const movieid = req.params.movieid;
  const sql =
    "Select m.movieid, m.name as movieName, m.year, m.plot, m.poster, a.actorid," +
    " a.name as actorName,p.producerid, p.name as prodName from movies m INNER JOIN" +
    " movieactor ma ON m.movieid = ma.movieid INNER JOIN actors a on ma.actorid" +
    " = a.actorid inner join movieproducer mp on m.movieid = mp.movieid" +
    " inner join producers p on mp.producerid = p.producerid WHERE m.movieid =" +
    movieid +
    ";";
  db.query(sql, (err, results, fields) => {
    if (err) {
      res.json({
        message: "failed",
      });
    } else {
      res.json({
        message: "success",
        data: results,
      });
    }
  });
});

router.post("/updatemovie", async (req, res) => {
  console.log(req.body);
  const moviedata = {};
  moviedata["movieid"] = req.body.movieid;
  moviedata["name"] = req.body.name;
  moviedata["year"] = req.body.year;
  moviedata["plot"] = req.body.plot;
  moviedata["poster"] = req.body.poster;
  db.query("INSERT INTO movies SET?", moviedata, (err, results, fields) => {
    if (err) {
      res.json({
        message: "Failed",
      });
    }
  });
  db.query(
    "Select movieid from movies where name ='" + moviedata.name + "';",
    (err, results, fields) => {
      if (err) {
        res.json({
          message: "failed",
        });
      } else {
        const movieid = results[0].movieid;
        db.query(
          "INSERT INTO movieproducer SET?",
          {
            movieid: movieid,
            producerid: req.body.producer,
          },
          (err, results, fields) => {
            if (err) {
              console.log("i m working");
              res.json({
                message: "failed",
              });
            } else {
              req.body.actors.forEach((element) => {
                db.query(
                  "INSERT INTO movieactor SET?",
                  {
                    movieid: movieid,
                    actorid: element,
                  },
                  (err, results, fields) => {
                    if (err) {
                      console.log("i m working 2");
                      res.json({
                        message: "failed",
                      });
                    }
                  },
                );
              });
              res.json({
                message: "success",
              });
            }
          },
        );
      }
    },
  );
});

router.delete("/delete/:movieid", (req, res) => {
  const movieid = req.params.movieid;
  db.query(
    "DELETE FROM movies WHERE movieid=" + movieid + ";",
    (err, results, fields) => {
      if (err) {
        res.json({
          message: "failed",
        });
      } else {
        res.json({
          message: "success",
        });
      }
    },
  );
});

router.get("/searchmovie/:searchvalue", (req, res) => {
  console.log(req.params);
  const sql =
    "Select m.movieid, m.name as movieName, m.year, m.plot, m.poster, a.actorid," +
    " a.name as actorName,p.producerid, p.name as prodName from movies m INNER JOIN" +
    " movieactor ma ON m.movieid = ma.movieid INNER JOIN actors a on ma.actorid" +
    " = a.actorid inner join movieproducer mp on m.movieid = mp.movieid" +
    " inner join producers p on mp.producerid = p.producerid WHERE m.name LIKE '%" +
    req.params.searchvalue +
    "%';";
  db.query(sql, (err, results, fields) => {
    if (err) {
      console.log(err);
      res.json({
        message: "failed",
      });
    } else {
      res.json({
        message: "success",
        data: results,
      });
    }
  });
});
module.exports = router;
